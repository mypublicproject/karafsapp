package ir.karafs.test.app.conf

class ModelItemDB_Column
{
    companion object
    {
        const val _ID = "_ID"
        const val firstName = "firstName"
        const val lastName = "lastName"
    }
}