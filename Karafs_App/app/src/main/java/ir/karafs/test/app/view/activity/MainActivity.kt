package ir.karafs.test.app.view.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.Observer
import ir.karafs.test.app.BR
import ir.karafs.test.app.R
//import ir.karafs.test.app.base.BaseActivity
import ir.karafs.test.app.base.BaseActivity
import ir.karafs.test.app.databinding.ActivityMainBinding
import ir.karafs.test.app.navigator.MainNavigator
import ir.karafs.test.app.viewModel.MainViewModel

class MainActivity: BaseActivity<ActivityMainBinding, MainViewModel>(), MainNavigator
{
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        
        viewModel!!.mNavigator = this
        
        dataBinding!!.toolbar.findViewById<TextView>(R.id.tvTitle).text = "سوال اول"
        dataBinding!!.toolbar.findViewById<ImageView>(R.id.imgBack).visibility = View.GONE
//        dataBinding!!.toolbar.findViewById<ImageView>(R.id.imgBack).setOnClickListener {  }
    
        viewModel!!.textInitLiveData.observe(this, Observer(viewModel!!::updateTextInit))
//        viewModel!!.textInitLiveData.observe(this, Observer{
//            it.let { s -> viewModel!!.updateTextInit(s) }
//        })
        viewModel!!.textPrmayshLiveData.observe(this, Observer(viewModel!!::updateTextPrmaysh))
    }
    
    override fun getViewModel(): Class<MainViewModel>
    {
        return MainViewModel::class.java
    }
    
    override val bindingVariable: Int
        get() = BR.viewModel
    
    override val layoutRes: Int
        get() = R.layout.activity_main
    
    override fun openQuizTwo()
    {
        Log.e("openQuizTwo", "click")
        startActivity(Intent(this, Quiz2Activity::class.java))
    }
}