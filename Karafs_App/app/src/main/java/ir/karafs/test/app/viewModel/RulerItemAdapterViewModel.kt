package ir.karafs.test.app.viewModel

import androidx.databinding.ObservableField
import ir.karafs.test.app.model.AdapterModel

//class RulerItemAdapterViewModel constructor(mWidthCM: Int, mWidthInch: Int, position: Int)
class RulerItemAdapterViewModel constructor(itemModel: AdapterModel, position: Int)
{
    var widthCMLiveData: ObservableField<Int> = ObservableField<Int>(itemModel.widthCM)
    var widthInchLiveData: ObservableField<Int> = ObservableField<Int>(itemModel.widthInch)
    var positionLiveData: ObservableField<Int> = ObservableField<Int>(position + 1)
}