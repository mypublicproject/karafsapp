package ir.karafs.test.app.view.activity

import android.os.Bundle
import android.util.DisplayMetrics
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import ir.karafs.test.app.BR
import ir.karafs.test.app.R
import ir.karafs.test.app.base.BaseActivity
import ir.karafs.test.app.databinding.ActivityQuiz3Binding
import ir.karafs.test.app.model.AdapterModel
import ir.karafs.test.app.navigator.Quiz3Navigator
import ir.karafs.test.app.singletone.SingletonContext
import ir.karafs.test.app.view.adapter.RulerHorizontalAdapter
import ir.karafs.test.app.view.adapter.RulerVerticalAdapter
import ir.karafs.test.app.viewModel.Quiz3ViewModel

class Quiz3Activity: BaseActivity<ActivityQuiz3Binding, Quiz3ViewModel>(), Quiz3Navigator
{
    private var mPixel:Int = 0
    private var mCM:Int = 0
    var adapterList : MutableList<AdapterModel> = mutableListOf()
    
    override val bindingVariable: Int
        get() = BR.viewModel
    
    override val layoutRes: Int
        get() = R.layout.activity_quiz3
    
    override fun getViewModel(): Class<Quiz3ViewModel>
    {
        return Quiz3ViewModel::class.java
    }
    
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
    
        viewModel!!.mNavigator = this
    
        dataBinding!!.toolbar.findViewById<TextView>(R.id.tvTitle).text = "سوال دوم"
        dataBinding!!.toolbar.findViewById<ImageView>(R.id.imgBack).setOnClickListener { finish() }
        
        setWidth()
        
        viewModel!!.adapterListLiveData.observe(this, Observer {

            viewModel!!::setObservableData

            dataBinding!!.rcHorizontal.layoutManager = LinearLayoutManager(SingletonContext.instance.context, LinearLayoutManager.HORIZONTAL, false)
            dataBinding!!.rcVertical.layoutManager = LinearLayoutManager(SingletonContext.instance.context, LinearLayoutManager.VERTICAL, false)
            dataBinding!!.rcHorizontal.itemAnimator = DefaultItemAnimator()
            dataBinding!!.rcVertical.itemAnimator = DefaultItemAnimator()
            dataBinding!!.rcHorizontal.adapter = RulerHorizontalAdapter(adapterList)
            dataBinding!!.rcVertical.adapter = RulerVerticalAdapter(adapterList)
        })
    }
    
    override  fun setWidth()
    {
        val dm = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(dm)
        
        mCM = (dm.widthPixels / (dm.widthPixels / dm.xdpi)).toInt()
        mPixel = (dm.widthPixels / (dm.widthPixels * 2.54 / dm.xdpi)).toInt()
    
        (0..14).asSequence().forEach {
        
            var adapterModel = AdapterModel(mCM!!, mPixel!!)
            adapterList.add(adapterModel)
        }
        viewModel!!.setLiveData(adapterList)
    }
}
