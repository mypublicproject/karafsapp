package ir.karafs.test.app.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import ir.karafs.test.app.BR
import ir.karafs.test.app.base.BaseAdapter
import ir.karafs.test.app.base.BaseViewHolder
import ir.karafs.test.app.databinding.AdapterQuiz3HorizontalBinding
import ir.karafs.test.app.model.AdapterModel
import ir.karafs.test.app.viewModel.RulerItemAdapterViewModel

class RulerHorizontalAdapter(val adapterModelList: MutableList<AdapterModel>?): BaseAdapter<BaseViewHolder, AdapterModel>()
{
    override fun setData(data: List<AdapterModel>?)
    {
        adapterModelList!!.addAll(data!!)
        notifyDataSetChanged()
    }
    
    fun onClearItems()
    {
        adapterModelList!!.clear()
    }
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder
    {
        val bindingHorizontal = AdapterQuiz3HorizontalBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        
        return RulerBinderHolder(bindingHorizontal)
    }
    
    override fun getItemCount(): Int = adapterModelList!!.size
    
    
    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) = holder.onBind(position)
    
    
    inner class RulerBinderHolder constructor(private val viewDataBinding: AdapterQuiz3HorizontalBinding) : BaseViewHolder(viewDataBinding.root)
    {
        override fun onBind(position: Int)
        {
            if (adapterModelList != null)
            {
                if (adapterModelList.isNotEmpty())
                {
                    val adapterModel = adapterModelList[position]
                    var viewModel = RulerItemAdapterViewModel(adapterModel, position)
                    viewDataBinding.itemViewModel = viewModel
                    viewDataBinding.setVariable(BR.viewModel, viewModel)
                    viewDataBinding.executePendingBindings()
                    
                    with(viewDataBinding)
                    {
    
                        val paramsW = RelativeLayout.LayoutParams(adapterModel.widthInch, 10)
    
                        rlRuler.layoutParams = paramsW
                    }
                    
                }
            }
        }
    }
}