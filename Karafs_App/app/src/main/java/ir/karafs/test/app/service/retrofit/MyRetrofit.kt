package ir.karafs.test.app.service.retrofit

import ir.karafs.test.app.service.url.Const
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

open class MyRetrofit
{
    companion object
    {
        fun getRetrofit(): Retrofit?
        {
            return Retrofit.Builder()
                    .baseUrl(Const.BASEURL)
                    .client(getOkHttp())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
        }
    
        fun getOkHttp(): OkHttpClient?
        {
            return OkHttpClient.Builder()
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .build()
        }
    }
    
}