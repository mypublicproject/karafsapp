package ir.karafs.test.app.base

import android.os.Bundle
import android.util.Log
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject

abstract class BaseActivity<D: ViewDataBinding, V: BaseViewModel<*>> : AppCompatActivity()
{
    var dataBinding: D? = null
    var viewModel: V? = null
    
    abstract val bindingVariable: Int
    
    lateinit var viewModelFactory: ViewModelProvider.Factory @Inject set
    
    @get:LayoutRes
    protected abstract val layoutRes: Int
    
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(getViewModel())
        dataBinding = DataBindingUtil.setContentView(this, layoutRes)
        dataBinding!!.setVariable(bindingVariable, viewModel)
        dataBinding!!.executePendingBindings()
        
        Log.e("==layoutRes==", layoutRes.toString())
        Log.e("==bindingVariable==", bindingVariable.toString())
        
    }
    
    protected abstract fun getViewModel(): Class<V>
    
}