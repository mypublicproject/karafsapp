package ir.karafs.test.app.viewModel

import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
//import ir.karafs.test.app.base.BaseViewModel
import ir.karafs.test.app.base.BaseViewModel
import ir.karafs.test.app.navigator.MainNavigator

class MainViewModel: BaseViewModel<MainNavigator>()
{
    private var row = 0
    private var rowStart = 0
    private var step = 4
    private var condition = true
    private var text = ""
    
    var textPrmaysh = ObservableField<String>()
    var textInit = ObservableField<String>()
    
    var textPrmayshLiveData = MutableLiveData<String>()
    var textInitLiveData = MutableLiveData<String>()
    
    
    init
    {
        var value = 0
        val n = 5
    
        val baseArray = Array(n) { IntArray(n) }
        //---------------fill value------------------
        for (i in 0 until n)
        {
            for (j in 0 until n)
            {
                baseArray[i][j] = value
                value ++
            }
        }
        //---------------fill value------------------
        textInitLiveData.value = baseArray.contentDeepToString().replace("], ", "]\n")
    
        while (row + step < 5 && row + step >= 0 && step >= - 4)
        {
            Log.e("---", "baseArray[" + row + "][" + (row + step) + "]: " + baseArray[row][row + step])
            text += baseArray[row][row + step].toString() + "    "
            if (step == 0 && row == 4)
            {
                condition = false
            }
            row ++
            if (condition)
            {
                if (row + step >= 5)
                {
                    text += "\n\n"
                    step --
                    row = rowStart
                }
            }
            else
            {
                if (row + step == 4 || row > 4)
                {
                    text += "\n\n"
                    step --
                    rowStart ++
                    row = rowStart
                }
            }
        }
        textPrmayshLiveData.value = text
    }
    
    fun updateTextInit(text: String)
    {
        Log.e("--updateTextInit--", "init")
        textInit.set(text)
    }
    
    fun updateTextPrmaysh(text: String)
    {
        textPrmaysh.set(text)
        Log.e("--updateTextPrmaysh--", "Prmaysh")
    }
    
    fun openNextQuiz()
    {
        Log.e("openNextQuiz", "click")
        mNavigator!!.openQuizTwo()
    }
}