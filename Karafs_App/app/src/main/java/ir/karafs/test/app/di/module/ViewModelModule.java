package ir.karafs.test.app.di.module;


import androidx.lifecycle.ViewModelProvider;

import dagger.Binds;
import dagger.Module;
import ir.karafs.test.app.viewModel.ViewModelFactory;

@Module
public abstract class ViewModelModule
{
    @Binds
    @SuppressWarnings("unused")
    abstract ViewModelProvider.Factory bindsViewModelFactory(ViewModelFactory viewModelFactory);
}
