package ir.karafs.test.app.db.dbModel;

import io.realm.RealmObject;

public class ModelDB extends RealmObject
{
    private int _ID;

    private String firstName;

    private String lastName;

    public int get_ID()
    {
        return _ID;
    }

    public void set_ID(int _ID)
    {
        this._ID = _ID;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }
}
