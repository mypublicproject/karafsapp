package ir.karafs.test.app.di.component;

import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import ir.karafs.test.app.di.module.ViewModelModule;

@Singleton
//@Component(modules = {ViewModelModule.class, AndroidInjectionModule.class})
@Component(modules = {ViewModelModule.class})
public interface ViewModelComponent
{

    @Component.Builder
    interface Builder
    {
        @BindsInstance
        Builder application(Application application);

        ViewModelComponent build();
    }

}
