package ir.karafs.test.app.view.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import io.realm.Realm
import io.realm.RealmConfiguration
import ir.karafs.test.app.BR
import ir.karafs.test.app.R
import ir.karafs.test.app.base.BaseActivity
import ir.karafs.test.app.databinding.ActivityQuiz2Binding
import ir.karafs.test.app.navigator.Quiz2Navigator
import ir.karafs.test.app.viewModel.Quiz2ViewModel

class Quiz2Activity: BaseActivity<ActivityQuiz2Binding, Quiz2ViewModel>(), Quiz2Navigator
{
    private var mRealm: Realm? = null
    get
    
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
    
        val realmConfiguration = RealmConfiguration.Builder()
                .schemaVersion(0)
//                .migration(new RealmMigrations())
                .build()
        Realm.setDefaultConfiguration(realmConfiguration)
        
        viewModel!!.mNavigator = this
        
        mRealm = Realm.getInstance(realmConfiguration)
    
        dataBinding!!.toolbar.findViewById<TextView>(R.id.tvTitle).text = "سوال دوم"
        dataBinding!!.toolbar.findViewById<ImageView>(R.id.imgBack).setOnClickListener { finish() }
    }
    
    override val bindingVariable: Int
        get() = BR.viewModel
    override val layoutRes: Int
        get() = R.layout.activity_quiz2
    
    override fun getViewModel(): Class<Quiz2ViewModel>
    {
        return Quiz2ViewModel::class.java
    }
    
    override fun openQuizThree()
    {
        startActivity(Intent(this, Quiz3Activity::class.java))
    }
    
    override fun onDestroy()
    {
        viewModel!!.onDestroyVM()
        super.onDestroy()
    }
}
