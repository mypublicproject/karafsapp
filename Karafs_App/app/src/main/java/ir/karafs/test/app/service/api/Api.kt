package ir.karafs.test.app.service.api

import io.reactivex.Single
import ir.karafs.test.app.service.model.ModelItem
import ir.karafs.test.app.service.url.Const
import retrofit2.http.GET

interface Api
{
    @GET(Const.GetList)
    fun getList(): Single<List<ModelItem?>?>?
}