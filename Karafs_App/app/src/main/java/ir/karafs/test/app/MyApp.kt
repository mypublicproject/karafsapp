package ir.karafs.test.app

import android.app.Application
import io.realm.Realm
import ir.karafs.test.app.singletone.SingletonContext
import ir.karafs.test.app.utilities.font.CustomViewWithTypefaceSupport
import ir.karafs.test.app.utilities.font.TextField
import uk.co.chrisjenx.calligraphy.CalligraphyConfig

class MyApp : Application()
{
    override fun onCreate()
    {
        super.onCreate()
        Realm.init(this)
        
        SingletonContext.instance.context = this
    
        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                .setDefaultFontPath("font/iran_sans_normal.ttf")
                .setFontAttrId(R.attr.fontPath)
                .addCustomViewWithSetTypeface(CustomViewWithTypefaceSupport::class.java)
                .addCustomStyle(TextField::class.java, R.attr.textFieldStyle)
                .build()
        )
    }
}