package ir.karafs.test.app.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ir.karafs.test.app.model.AdapterModel

class RulerAdapterViewModel(val adapterModelList: List<AdapterModel>): ViewModel()
{
    lateinit var adapterModelMutableLive : MutableLiveData<List<AdapterModel>>
    
    init
    {
        
        adapterModelMutableLive = MutableLiveData<List<AdapterModel>>(adapterModelList)
    }
    
    fun getAdapterModelMutableLive() = adapterModelMutableLive as LiveData<List<AdapterModel>>
}