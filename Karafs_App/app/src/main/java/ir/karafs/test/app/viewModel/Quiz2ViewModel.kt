package ir.karafs.test.app.viewModel

import android.util.Log
import android.widget.Toast
import androidx.databinding.ObservableField
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.RealmResults
import ir.karafs.test.app.base.BaseViewModel
import ir.karafs.test.app.db.dbImpl.InsertDBImpl
import ir.karafs.test.app.db.dbImpl.QueryDBImpl
import ir.karafs.test.app.db.dbModel.ModelDB
import ir.karafs.test.app.navigator.Quiz2Navigator
import ir.karafs.test.app.service.api.Api
import ir.karafs.test.app.service.model.ModelItem
import ir.karafs.test.app.service.retrofit.MyRetrofit
import ir.karafs.test.app.singletone.SingletonContext

class Quiz2ViewModel: BaseViewModel<Quiz2Navigator>()
{
    var progressVisibility = ObservableField<Boolean>()
    var text = ObservableField<String>()
    
    private var mRealm: Realm? = null
    private var content: String = ""
    
    init
    {
        progressVisibility.set(true)
        text.set("Loading Data ...")
    
        val realmConfiguration = RealmConfiguration.Builder()
                .schemaVersion(0)
//                .migration(new RealmMigrations())
                .build()
        Realm.setDefaultConfiguration(realmConfiguration)
    
        mRealm = Realm.getInstance(realmConfiguration)
    
        val api: Api = MyRetrofit.getRetrofit()!!.create(Api::class.java)
        
        disposable!!.add(
                api.getList()!!
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({ response: List<ModelItem?>? -> this.onResponse(response) }) { err: Throwable? -> onFailure(err) }
        )
    
    }
    
    
    private fun onResponse(response: List<ModelItem?>?)
    {
        progressVisibility.set(false)
        Log.e("-response size-", "size: " + response?.size)
        if (mRealm !!.where(ModelDB::class.java).findAll().isEmpty())
        {
            disposable!!.add(InsertDBImpl.InsertEntity(response, mRealm))
            
        }
        printQuery()
    }
    
    private fun onFailure(err: Throwable?)
    {
        progressVisibility.set(false)
        Toast.makeText(SingletonContext.instance.context, err?.message, Toast.LENGTH_SHORT).show()
    }
    
    private fun printQuery()
    {
        if (mRealm !!.where(ModelDB::class.java).findAll().isEmpty())
        {
            text.set("DataBase is Empty!")
        }
        else
        {
            val dbList: RealmResults<ModelDB> = mRealm !!.where(ModelDB::class.java).findAll()
            
            disposable!!.add(Observable.fromIterable(dbList)
                    .doOnNext { ModelDB -> content += QueryDBImpl.GetQuery(ModelDB, mRealm) }
                    .doOnComplete { text.set(content) }
                    .subscribe()
            )
        }
    }
    
    
    fun openNextQuiz()
    {
        Log.e("openNextQuiz", "click")
        mNavigator!!.openQuizThree()
    }
    
    fun onDestroyVM()
    {
        mRealm!!.close()
    }
}