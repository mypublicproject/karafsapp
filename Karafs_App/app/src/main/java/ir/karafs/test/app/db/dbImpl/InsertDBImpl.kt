package ir.karafs.test.app.db.dbImpl

import android.util.Log
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.realm.Realm
import io.realm.exceptions.RealmException
import ir.karafs.test.app.db.dbModel.ModelDB
import ir.karafs.test.app.service.model.ModelItem

open class InsertDBImpl
{
    companion object
    {
        fun InsertEntity(response: List<ModelItem?>?, mRealm: Realm?): Disposable
        {
            return Observable.fromIterable(response)
                    .subscribe{ modelItem ->
                        mRealm!!.executeTransaction { realm: Realm ->
                            try
                            {
                                var itemDB: ModelDB = realm.createObject(ModelDB::class.java)
                                var maxId = 0
                                if (! realm.where(ModelDB::class.java).findAll().isEmpty())
                                {
                                    maxId = realm.where(ModelDB::class.java).max("_ID")!!.toInt()
                                }
                                itemDB._ID = maxId + 1
                                itemDB.firstName = modelItem?.firstName
                                itemDB.lastName = modelItem?.lastName
                                Log.e("---DB---", "Id " + (maxId + 1) + ", " + modelItem?.firstName + " " + modelItem?.lastName + " was added.")
                            }
                            catch (e : RealmException)
                            {
                                Log.e("-RealmException-", "Exception: " + e.message)
                                e.printStackTrace()
                            }
                        }
                    }
        }
    }
}