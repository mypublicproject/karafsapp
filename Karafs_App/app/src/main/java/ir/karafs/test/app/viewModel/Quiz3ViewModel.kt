package ir.karafs.test.app.viewModel

import android.util.DisplayMetrics
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableList
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ir.karafs.test.app.base.BaseViewModel
import ir.karafs.test.app.model.AdapterModel
import ir.karafs.test.app.navigator.Quiz2Navigator
import ir.karafs.test.app.navigator.Quiz3Navigator

class Quiz3ViewModel: BaseViewModel<Quiz3Navigator>()
{
    var adapterListLiveData = MutableLiveData<List<AdapterModel>>()
    var adapterListObs : ObservableList<AdapterModel> = ObservableArrayList()
    
    fun setLiveData(adapterList : MutableList<AdapterModel>)
    {
        adapterListLiveData.value = adapterList
    }
    
    fun setObservableData(adapterList : MutableList<AdapterModel>)
    {
        adapterListObs.addAll(adapterList)
    }
    
    
}