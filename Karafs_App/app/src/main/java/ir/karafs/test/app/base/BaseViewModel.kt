package ir.karafs.test.app.base

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

abstract class BaseViewModel<N> : ViewModel()
{
    var mNavigator: N? = null
    
    var disposable: CompositeDisposable? = null
    
    init
    {
        disposable = CompositeDisposable()
    }
    
    override fun onCleared()
    {
        disposable !!.dispose()
        super.onCleared()
    }
    
//    fun getCompositeDisposable(): CompositeDisposable?
//    {
//        return mCompositeDisposable
//    }

//    fun getNavigator(): N
//    {
//        return mNavigator
//    }
//
//    fun setNavigator(mNavigator: N)
//    {
//        this.mNavigator = mNavigator
//    }

}