package ir.karafs.test.app.utilities.font

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import ir.karafs.test.app.R

class TextField(context:Context, attrs: AttributeSet) : AppCompatTextView(context, attrs, R.attr.fontPathCalligraphyConfig)
{
}