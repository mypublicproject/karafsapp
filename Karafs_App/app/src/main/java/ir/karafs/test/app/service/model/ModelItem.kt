package ir.karafs.test.app.service.model

import com.google.gson.annotations.SerializedName

data class ModelItem(
        @SerializedName("firstName")
        var firstName: String = "",

        @SerializedName("lastName")
        var lastName: String = ""
)
{
}