package ir.karafs.test.app.binding

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import ir.karafs.test.app.model.AdapterModel
import ir.karafs.test.app.view.adapter.RulerHorizontalAdapter
import ir.karafs.test.app.view.adapter.RulerVerticalAdapter

@BindingAdapter(value = ["bind:recyclerAdapter"])
fun bindItemAdapter(recyclerView: RecyclerView, items: List<AdapterModel>?)
{
    val adapter = recyclerView.adapter
    
    items?.let {
        if (adapter is RulerHorizontalAdapter)
        {
            adapter.setData(it)
            recyclerView.adapter = adapter
        }
        else if (adapter is RulerVerticalAdapter)
        {
            adapter.setData(it)
            recyclerView.adapter = adapter
        }
    }
}