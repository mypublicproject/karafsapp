package ir.karafs.test.app.db.dbImpl

import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.realm.Realm
import io.realm.RealmResults
import ir.karafs.test.app.conf.ModelItemDB_Column
import ir.karafs.test.app.db.dbModel.ModelDB

class QueryDBImpl
{
    companion object
    {
        fun GetQuery(mModelItemDB: ModelDB, mRealm: Realm?): String
        {
            var content = ""
            var containerFirst: String? = mModelItemDB.lastName
            var containerLast: String? = mModelItemDB.lastName
            if (mModelItemDB.lastName!!.contains("-"))
            {
                containerFirst = mModelItemDB.lastName!!.split("-").get(0)
                containerLast = mModelItemDB.lastName!!.split("-").get(1)
            }
            val foundListDB: RealmResults<ModelDB> = mRealm !!.where(ModelDB::class.java)
                    .beginGroup()
                    .notEqualTo(ModelItemDB_Column._ID, mModelItemDB._ID)
                    .endGroup()
                    .and()
                    .beginGroup()
                    .equalTo(ModelItemDB_Column.lastName, mModelItemDB.lastName)
                    .or()
                    .beginsWith(ModelItemDB_Column.lastName, mModelItemDB.lastName!!.toString() + "-")
                    .or()
                    .endsWith(ModelItemDB_Column.lastName, "-" + mModelItemDB.lastName)
                    .or()
                    .contains(ModelItemDB_Column.lastName, containerFirst)
                    .or()
                    .contains(ModelItemDB_Column.lastName, containerLast)
                    .endGroup()
                    .findAll()
            if (foundListDB.isEmpty())
            {
                content += "${mModelItemDB.firstName.toString()} is related to no one.\n\n"
            }
            else
            {
                var names = ""

                Observable.fromIterable(foundListDB)
                        .doOnNext {ModelDB ->
                            if (names == "")
                            {
                                names += ModelDB.firstName
                            }
                            else
                            {
                                names += " , " + ModelDB.firstName
                            }
                        }
                        .doOnComplete {
                            if (names.contains(","))
                            {
                                val index: Int = names.lastIndexOf(",")
                                names = names.substring(0, index) + "&" + names.substring(index + 1)
                            }
                            content += mModelItemDB.firstName.toString() + " is related to " + names + ".\n\n"
                        }
                        .subscribe()
            }

            return content
        }
    }
}