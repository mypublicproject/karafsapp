package ir.karafs.test.app.model

data class AdapterModel(
        var widthCM: Int,
        var widthInch: Int
)